import React, { Component } from 'react';
import { Card, Spinner } from 'react-bootstrap';
import * as filmsService from '../services/filmsService';


export default class pages extends Component {
  constructor(props) {
    super(props);

    this.state = {
      films: []
    }
  }

  componentDidMount() {
    filmsService.getFilms().then(films => {
      this.setState({ films, ...films });
    });
  }

  render() {
    return (
      <>
        <h5>Lista de filmes: </h5>

        {this.state.films.length < 1 && 
          <div style={{width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
            <Spinner animation="grow" />
          </div>
        }
      
        {this.state.films.map(film =>
          <Card key={film.episode_id} style={{margin: '30px 0'}}>
            <Card.Header as="h5">{film.title}</Card.Header>
            <Card.Body>
              <Card.Text>{film.opening_crawl}</Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">Dirigido por: {film.director}</small>
              <small className="text-muted" style={{float: 'right'}}>Data de estreia: {film.release_date}</small>
            </Card.Footer>
          </Card>
          )
        }
      </>
    );
  }
}