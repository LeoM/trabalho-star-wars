import React from 'react';
import { HashRouter, Switch, Route } from 'react-router-dom';
import Header from './components/shared/Header';
import Home from './screens/Home';
import Films from './screens/Films';

function App() {
  return (
    <div className="container-principal">
      <HashRouter>
        <Header/>
        <Switch>
          <Route path='/' exact={true} component={Home} />
          <Route path='/films' component={Films} />
        </Switch>
      </HashRouter>
    </div>
  );
}

export default App;
