import axios from 'axios';

export default axios.create({
  baseURL: `https://swapi.co/api/`,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
  }
});