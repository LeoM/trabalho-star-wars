import api from '../configs/api';

export const getFilms = () => {
    return api.get('/films').then(res => {
        return res.data.results;
    }).catch(err => console.log(err));
}